% help ORCS

% [BestNest, BestFitness, EvaluationsCount] = ORCS(@Ackley, 5, -32, 32, 0, 25000);
% [BestNest, BestFitness, EvaluationsCount] = ORCS(@Griewank, 5, -600, 600, 0, 25000);
% [BestNest, BestFitness, EvaluationsCount] = ORCS(@Rastrigin, 5, -5.12, 5.12, 0, 25000);
[BestNest, BestFitness, EvaluationsCount] = ORCS(@Rosenbrock, 5, -50, 50, 0, 25000);
% [BestNest, BestFitness, EvaluationsCount] = ORCS(@Sphere, 5, -100, 100, 0, 25000);
