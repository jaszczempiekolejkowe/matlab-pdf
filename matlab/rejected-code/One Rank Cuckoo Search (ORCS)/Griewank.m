function y = Rosenbrock(x)
% Rosenbrock function
% Matlab Code by A. Hedar (Sep. 29, 2005).
% Last updated by Ahmed Tawfik (Apr. 30, 2013).
n = size(x,2);
sum = 0;
for j = 1:n-1;
    sum = sum+100*(x(j)^2-x(j+1))^2+(x(j)-1)^2;
end
y = sum;