function [solution] = mCuckoo(NoPob) 
if nargin<1, 
    % Number of poblation 
    NoPob=10; 
end 
 
N_IterTotal=100; 
FunctionCalls = 0; 
%paremeters 
dim     = 1; 
phase   = 1; 
phaseIte= [0.5,0.9,1.01]; 
pa      = 0.25; 
 
%graph stuff 
plotGraph = 2; 
if plotGraph ~= 0 
    X=0:1/100:1; 
    for l=1:size(X,2) 
     Y(l)=Func1D([X(l)]); 
    end 
end 
 
% Random initial solutions 
pob = rand(NoPob,dim); 
% Eval Fitness 
fitness = evalPob(pob); 
% Initialice the memory 
[mem,bestSol,bestFit,worstF] = memUpdate(pob,fitness, [], zeros(1,dim), 100000000, 0, phase); 
 
for ite = 1:N_IterTotal 
    pob = get_cuckoos(pob,bestSol,0,1); 
    fitness = evalPob(pob); 
    [mem,bestSol,bestFit,worstF] = memUpdate(pob,fitness,mem,bestSol,bestFit,worstF,phase); 
    if plotGraph == 2 
        graph2d(X, Y, pob, fitness, mem, bestSol,bestFit); 
    end 
     
    % Discovery and randomization 
    pob = empty_nests(pob,0,1,pa) ; 
    fitness = evalPob(pob); 
    [mem,bestSol,bestFit,worstF] = memUpdate(pob,fitness,mem,bestSol,bestFit,worstF,phase); 
    if plotGraph == 2 
        graph2d(X, Y, pob, fitness, mem, bestSol,bestFit); 
    end 
     
    % Choose new poblation 
    pob = get_best_nest(pob,mem,pa); 
 
    % Update the counter again 
    FunctionCalls = FunctionCalls + NoPob * 2; 
     
    if ite/N_IterTotal > phaseIte(phase) 
        phase = phase + 1; 
        mem = cleanMemory(mem); 
    end 
     
end 
solution = mem; 
if plotGraph == 1 
    hold off 
    plot(X,Y); 
    hold on 
    plot(mem(:,1),mem(:,2),'or'); 
end 
 
function [ r, x ] = Func1D(vector) 
    x = vector; 
    x = x * 1000 - 500; %scale to the bounds 
    [~,m]=size(vector); 
    r = 418.9829*m + sum( -x.*sin(sqrt(abs(x))) ); 
 
function f = evalPob(pob) 
    f = zeros(size(pob,1),1); 
    for i=1:size(pob,1) 
        f(i) = Func1D(pob(i,:)); 
    end 
     
function nest = get_cuckoos(nest, best, Lb, Ub) 
    n=size(nest,1); 
    beta=3/2; 
    sigma=(gamma(1+beta)*sin(pi*beta/2)/(gamma((1+beta)/2)*beta*2^((beta-1)/2)))^(1/beta); 
 
    for j=1:n, 
        s=nest(j,:); 
        u=randn(size(s))*sigma; 
        v=randn(size(s)); 
        step=u./abs(v).^(1/beta); 
        stepsize=0.01*step.*(s-best); 
        s=s+stepsize.*randn(size(s)); 
        % Apply simple bounds/limits 
        nest(j,:)=simplebounds(s,Lb,Ub); 
    end 
 
function pob = get_best_nest(pob, mem, pa) 
    %change the poblation 
    [m,~]=size(mem); 
    if m > size(pob,1) 
        for i=1:size(pob,1) 
            pob(i,:) = mem(i,1:end-1); 
        end 
    else 
        for i=1:m 
            pob(i,:) = mem(i,1:end-1); 
        end 
        for i=m+1:size(pob,1) 
            for j=1:size(pob,2) 
                if rand > pa 
                    pob(i,j) = pob(i-m,j); 
                else 
                    pob(i,j) = rand; 
                end 
            end 
        end 
    end 
 
function new_nest=empty_nests(nest,Lb,Ub,pa) 
    % A fraction of worse nests are discovered with a probability pa 
    n=size(nest,1); 
    % Discovered or not -- a status vector 
    K=rand(size(nest))>pa; 
 
    stepsize=rand*(nest(randperm(n),:)-nest(randperm(n),:)); 
    new_nest=nest+stepsize.*K; 
    for j=1:size(new_nest,1) 
        s=new_nest(j,:); 
        new_nest(j,:)=simplebounds(s,Lb,Ub);   
    end 
 
function s = simplebounds(s,Lb,Ub) 
    % Apply the lower bound 
    ns_tmp=s; 
    I=ns_tmp<Lb; 
    ns_tmp(I)=0; 
 
    % Apply the upper bounds  
    J=ns_tmp>Ub; 
    ns_tmp(J)=1; 
     
    % Update this new move  
    s=ns_tmp; 
 
function graph2d(X, Y, pob, fit, mem, bestSol, bestFit) 
    hold off 
    plot(X,Y); 
    hold on 
    plot(mem(:,1),mem(:,2),'ob'); 
    plot(pob(:,1),fit(:,1),'*r'); 
    plot(bestSol,bestFit,'og'); 
    drawnow 
     
%--------------Multimodal Functions---------------% 
function mem = cleanMemory(mem) 
    [m,n] = size(mem); 
    MemL = mem(:,1:end-1); %Take the Positions 
    rdp = zeros(m,m);      %Distance Matrix 
    rdppi = zeros(m,1); 
    for i=1:m 
        %for each entry in memory (sorted by fitness) 
        for j=1:m 
            %Calculate the distance with others 
            if i~= j 
                rdp(i,j) = norm(MemL(i,:)-MemL(j,:)); 
            end 
        end 
         
        %Sort a new matrix by distance 
        [~, jj]=sort(rdp(i,:)); 
        for j=1:m 
            %for each one until the condition has found 
            if rdppi(i) ==0 
                temp = Func1D(MemL(i,:)+(MemL(jj(j),:)-MemL(i,:))*0.5); 
                maxx = max([mem(jj(j),end),mem(i,end)]); 
                 
                %check the condition 
                if temp > maxx 
                    %Take 85% of the distance as clear ratio 
                    rdppi(i) = rdp(i,jj(j))*0.85; 
                end 
            end 
        end 
 
    end 
 
    %Make the clean process with the calculated ratios 
    cTemp=0; 
    badInd = zeros(m,1); %storage the entrys with redundant information 
    for i=1:m 
        if badInd(i) == 0 
            %check witch one are closer than the ratio 
            ind = rdp(i,:) < rdppi(i); 
            %stores that indices 
            badInd(ind) = badInd(ind) + 1; 
            %save the predominants 
            cTemp = cTemp+1; 
            NewMem(cTemp,1:n) = mem(i,:); 
        end 
    end 
    mem = NewMem; 
     
function [mem,bestSol,bestFit,worstF] = memUpdate(pob,fit,mem,bestSol,bestFit,worstF, phase) 
    %Update the best fitness so-far 
    [~, p] = min(fit); 
    if fit(p) < bestFit 
        bestFit = fit(p); 
        bestSol = pob(p,:); 
    end 
    %Update the worst fitness so-far 
    [~, p] = max(fit); 
    if fit(p) > worstF 
        worstF = fit(p); 
    end 
     
    [m,~] = size(mem); 
    if m == 0 
        %Initialice Memory (only in the fist iteration) 
        mem = zeros(1, size(pob,2) + 1); 
        mem(1, :) = [bestSol, bestFit]; 
    else 
        %Update Memory 
        [m,~] = size(mem); 
        worstMem = mem(m,end); %Worst Fitness inside memory 
        for i=1:size(pob,1) 
            %For each one in population 
            MemL = mem(:,1:end-1); %Positions in memory 
            [m,~] = size(mem); 
            if fit(i) <= worstMem 
                %Better than the worst inside memory 
                rd=zeros(m,1); 
                for j=1:m 
                    %Distance with all the entrys in memory 
                    rd(j) = sum((pob(i,:)-MemL(j,:)).^2); 
                end 
                %Takes the shortest distance 
                [~,ii]=min(rd); 
                dd = sqrt(rd(ii))/size(pob,2); %normalized distance 
                if rand < dd^phase 
                    %New entry 
                    mem(end+1, 1:end) = [pob(i,:), fit(i)]; 
                else 
                    if fit(i) < mem(ii,end) 
                        %Remplace 
                        mem(ii, :) = [pob(i,:), fit(i)]; 
                    end 
                end 
                 
            else 
                %not better than the worst inside memory 
                if 0.3 + rand < (1-(fit(i)-bestFit)/(worstF-bestFit)) 
                    rd=zeros(m,1); 
                    for j=1:m 
                        %Distance with all the entrys in memory 
                        rd(j) = sum((pob(i,:)-MemL(j,:)).^2); 
                    end 
                    %Takes the shortest distance 
                    [~,ii]=min(rd); 
                    dd = sqrt(rd(ii))/size(pob,2); %normalized distance 
                    if rand < dd^phase 
                        %new entry 
                        mem(end+1, 1:end) = [pob(i,:), fit(i)]; 
                    end 
                end 
            end 
        end 
        %sort memory by fitness 
        mem = sortrows(mem,size(mem,2)); 
 
        bestSol = mem(1,1:end-1); 
        bestFit = mem(1,end); 
    end 